import React from "react";
import { HttpRequest } from "../../Service/HttpRequest";
import { Modal, Button, Form } from "react-bootstrap";
class Books extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Books: [],
      title: "",
      description: "",
      id: "",
      show1: false,
      show2: false,
      search_key: "",
    };
    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeId = this.handleChangeId.bind(this);
    this.handleChangeSearch = this.handleChangeSearch.bind(this);
    this.parentCmp = this;
  }
  showModal_1 = () => {
    this.hideModal_2();
    this.setState({ show1: true });
  };

  hideModal_1 = () => {
    this.setState({ show1: false });
  };
  showModal_2 = (book) => {
    var id = book.currentTarget.value;
    this.hideModal_1();
    this.setState({ show2: true });

    this.setState({ id: book.currentTarget.value });

    var to_update_book = this.state.Books.filter(function (item) {
      return item["id"] == id;
    })[0];

    this.setState({ title: to_update_book["title"] });
    this.setState({ description: to_update_book["description"] });
  };

  hideModal_2 = () => {
    this.setState({ show2: false });
  };
  handleChangeTitle(event) {
    this.setState({ title: event.target.value });
  }
  handleChangeDescription(event) {
    this.setState({ description: event.target.value });
  }
  handleChangeId(event) {
    this.setState({ id: event.target.value });
  }
  handleChangeSearch(event) {
    this.setState({ search_key: event.target.value });
    this.get_books();
  }
  componentDidMount() {
    this.get_books();
  }

  get_books() {
    new HttpRequest().get(this.state.search_key).then((res) => {
      const Books = res.data;

      this.setState({ Books });
    });
  }
  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  add_book = () => {
    var new_book = { title: "", description: "" };
    new_book["title"] = this.state.title;
    new_book["description"] = this.state.description;

    new HttpRequest().post(new_book).then(
      (res) => {
        this.get_books();
        window.toastr.success("success add book");
        this.hideModal_1();
        this.setState({ description: "" });
        this.setState({ title: "" });
      },
      (error) => {
        window.toastr.error("error add book");
      }
    );
  };
  update_book = () => {
    var new_book = { title: "", description: "" };
    new_book["title"] = this.state.title;
    new_book["description"] = this.state.description;

    new HttpRequest().put(this.state.id, new_book).then(
      (res) => {
        this.get_books();
        window.toastr.success("success update book");
        this.hideModal_2();
        this.setState({ description: "" });
        this.setState({ title: "" });
      },
      (error) => {
        window.toastr.error("error update book");
      }
    );
  };

  delete_book = (book) => {
    var p = this.parentCmp;
    var id = book.currentTarget.value;

    window.$.confirm({
      title: "Confirmation!",
      content: "you are sure to delete this book!",
      buttons: {
        confirm: function () {
          window.$.alert("Confirmed");
          new HttpRequest().delete(id).then(
            (res) => {
              p.get_books();
              window.toastr.success("success delete book");
            },
            (error) => {
              window.toastr.error("error delete book");
            }
          );
        },
        cancel: function () {
          window.$.alert("Canceled");

          window.toastr.error("cancel delete book");
        },
      },
    });
  };
  render() {
    return (
      <div>
        <div>
          <div class="wrapper">
            <div>
              <section class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1>Books Table</h1>
                    </div>
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                          <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Books Table</li>
                      </ol>
                    </div>
                  </div>
                </div>
              </section>

              <section class="content">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-header">
                          <div class="card-title">
                            Add New Book
                            <button
                              type="button"
                              class="btn btn-block btn-info "
                              onClick={this.showModal_1}
                            >
                              <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                          </div>

                          <div class="card-tools">
                            <div
                              class="input-group input-group-sm"
                              style={{ width: "150px;" }}
                            >
                              <input
                                type="text"
                                name="table_search"
                                class="form-control float-right"
                                placeholder="Search"
                                value={this.state.search_key}
                                onInput={this.handleChangeSearch}
                              />

                              <div class="input-group-append">
                                <button type="submit" class="btn btn-default">
                                  <i class="fas fa-search"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="card-body table-responsive p-0">
                          <table class="table table-hover text-nowrap">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Latest Update</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>{this.renderTableData()}</tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        <Modal show={this.state.show1}>
          <Modal.Header>
            <Modal.Title>Add New Book</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="formBasicAPI">
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Title"
                value={this.state.title}
                onChange={this.handleChangeTitle}
              />
              <Form.Text className="text-muted">short title .</Form.Text>
            </Form.Group>
            <Form.Group controlId="formBasicAPI">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows="3"
                placeholder="Enter ..."
                value={this.state.description}
                onChange={this.handleChangeDescription}
              />
              <Form.Text className="text-muted">full description .</Form.Text>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.hideModal_1}>
              Close
            </Button>
            <Button variant="primary" onClick={this.add_book}>
              Save
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.show2}>
          <Modal.Header>
            <Modal.Title>Update Book</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="formBasicAPI">
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Title"
                value={this.state.title}
                onChange={this.handleChangeTitle}
              />
              <Form.Text className="text-muted">short title .</Form.Text>
            </Form.Group>
            <Form.Group controlId="formBasicAPI">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows="3"
                placeholder="Enter ..."
                value={this.state.description}
                onChange={this.handleChangeDescription}
              />
              <Form.Text className="text-muted">full description .</Form.Text>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.hideModal_2}>
              Close
            </Button>
            <Button variant="primary" onClick={this.update_book}>
              Save
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

  renderTableData() {
    return this.state.Books.map((Books, index) => {
      const { id, title, description, date } = Books; //destructuring
      return (
        <tr>
          <td>{id}</td>
          <td>{title}</td>
          <td> {description.slice(0, 20)}....</td>
          <td> {date.slice(0, 10)}</td>
          <td>
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <button
                class="btn btn-success "
                onClick={this.showModal_2}
                value={id}
              >
                <input
                  type="radio"
                  name="options"
                  id="option1"
                  autocomplete="off"
                  checked
                />
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
              </button>

              <button
                class="btn btn-danger"
                value={id}
                onClick={this.delete_book}
              >
                <input
                  type="radio"
                  name="options"
                  id="option3"
                  autocomplete="off"
                />
                <i class="fa fa-trash" aria-hidden="true"></i>
              </button>
            </div>
          </td>
        </tr>
      );
    });
  }
}

export default Books;
