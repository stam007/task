import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Books from "./Components/Books/Books";

function App() {
  return (
    <div>
      <Books />
    </div>
  );
}

export default App;
