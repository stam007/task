import React from "react";
import axios from "axios";
export class HttpRequest extends React.Component {
  get(key) {
    return axios({
      method: "get",

      url: "http://127.0.0.1:8000/books/?search=" + key,
      data: {},
    });
  }
  post(data) {
    return axios({
      method: "post",

      url: "http://127.0.0.1:8000/books/",
      data: data,
    });
  }

  delete(id) {
    return axios({
      method: "delete",
      url: "http://127.0.0.1:8000/books/" + id + "/",
      data: {},
    });
  }
  put(id, data) {
    return axios({
      method: "put",
      url: "http://127.0.0.1:8000/books/" + id + "/",
      data: data,
    });
  }
}
