from django.shortcuts import render
from rest_framework import viewsets
from myapp.models import *
from myapp.serializers import *
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters


class BooksViewSet(viewsets.ModelViewSet):
    queryset = Books.objects.all()
    serializer_class = BooksSerializer

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ['title', 'description', 'date']
    filterset_fields = ['description']
