from django.db import models
from datetime import datetime
# Create your models here.


class Books(models.Model):
    title = models.TextField(blank=True, null=True, default=None)
    description = models.TextField(blank=True, null=True, default=None)
    date = models.DateTimeField(blank=True, null=True, default=datetime.now())
