
from rest_framework import serializers
from myapp.models import *


class BooksSerializer(serializers.ModelSerializer):

    class Meta:
        model = Books
        fields = '__all__'
